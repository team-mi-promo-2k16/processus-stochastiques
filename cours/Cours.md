# Introduction

On va voir dans le cours, pour commencer :

* Variables aléatoires
* Simulation de ces variables

Pas de réel aléatoire dans une machine, car elle est déterministe -> Pseudo-aléatoire

Tous les langages implémentent un fonction permettant de générer un nombre
pseudo-aléatoire, simulant une variable aléatoire uniforme (le plus souvent sur $[0; 1]$).

Fonction de répartition de U_{[0; 1]} :

$$F_{U} = P(U \leq x) = x si 0 \leq x \leq 1$$



# TD 1



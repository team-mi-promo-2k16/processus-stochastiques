import random

def random_tuples(n):
    for _ in range(n):
        yield random.random(), random.random()

def MonteCarlo(n):
    return 4 * sum(1 for x, y in random_tuples(n) if x ** 2 + y ** 2 < 1) / n

if __name__ == '__main__':
    print(MonteCarlo(1000000))


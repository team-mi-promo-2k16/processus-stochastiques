import random

def Binomial(n, p):
    return sum(1 for x in range(n) if random.random() < p)

if __name__ == '__main__':
    print(Binomial(25, 0.5))


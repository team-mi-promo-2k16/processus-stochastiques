module DiscreteVariables where

import System.Random
import Control.Monad

-- Exercice 1
dice :: IO Int
dice = getStdRandom $ randomR (1, 6)

-- Exercice 2
simulPi :: Integer -> IO Double
simulPi n = do
  nc <- foldM 
        (\s _ -> do
          x <- getStdRandom random :: IO Double
          y <- getStdRandom $ random
          return $ s + (if x ** 2 + y ** 2 < 1 then 1 else 0)
        ) 0 [1..n]
  return $ 4.0 * nc / (fromInteger n :: Double)
  
-- Exercice 3
binomial :: Integer -> Double -> IO Integer
binomial n p = foldM (\acc _ -> bernouilli p >>= (return . (acc +))) 0 [1..n]
    where bernouilli :: Double -> IO Integer 
          bernouilli p = do
            r <- (getStdRandom random) :: IO Double
            return $ if r < p then 1 else 0
 
-- Exercice 4
geometric :: Double -> IO Integer
geometric p = do
  f 1
  where f :: Integer -> IO Integer 
        f n = do
          r <- (getStdRandom random) :: IO Double
          if r < p then return n
          else f (n + 1)

main :: IO ()
main = do
  newStdGen

  putStrLn "Dice roll: "
  dice >>= print

  putStrLn "Pi (10000 iterations): "
  simulPi 10000 >>= print

  putStrLn "Binomial (n = 10000, p = 0.42):"
  binomial 10000 0.42 >>= print
  
  putStrLn "Geometric (p = 0.66):"
  geometric 0.66 >>= print
  
  